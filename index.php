<html>
<title>Sistem Penjadwalan Dosen</title>
<head>
<link rel="stylesheet" href="style/materialize.min.css" />
</head>
<body>
<nav>
        <div class="nav-wrapper blue">
        <div class="container">
          <a href="index.php" class="brand-logo center white-text">WEBSITE JADWAL</a>
        </div>
        </div>
</nav>
 	<div class="row">
		<div class="col-md-8 col-md-offset-2"> 
			
			<br>
			<table class="table table-bordered">
				<tr>
					<th>
						No 
					</th>
					<th>
						Nama Dosen
					</th>
                    <th>
						Jadwal
					</th>
                    <th>
						Matakuliah
					</th>
					<th>
						Kelas
					</th>
					<th>
						Prodi
					</th>
					<th>
						Fakultas
					</th>
				</tr>
				<!-- sql ini berfungsi sebagai penggabung antara tabel satu dengan tabel yang lain  -->
					<?php
						include"koneksi.php";
						$no = 1;
                        $sql= "SELECT * FROM dosen INNER JOIN jadwal_kelas ON dosen.id_dosen=jadwal_kelas.id_dosen 
						INNER JOIN kelas ON kelas.id_kelas=jadwal_kelas.id_kelas";
						$data = mysqli_query ($koneksi, $sql);
						while ($row = mysqli_fetch_array ($data)){
                    ?>
                <tr>
					<td>
						<?php echo $no++; ?>
					</td>
					<td>
						<?php echo $row['nama_dosen']; ?>
					</td>
					<td>
						<?php echo $row['jadwal']; ?>
					</td>
					<td>
						<?php echo $row['matakuliah']; ?>
					</td>
					<td>
						<?php echo $row['nama_kelas']; ?>
					</td>
                    <td>
						<?php echo $row['prodi']; ?>
					</td>
					<td>
						<?php echo $row['fakultas']; ?>
					</td>
				</tr>   
				
				<?php
					}
				?>
			</table>
			<p>
				<center>
				<a class="btn blue" href="dosen.php">Edit Data Dosen</a>
                <a class="btn blue" href="kelas.php">Edit Data Kelas</a>
                <a class="btn blue" href="jadwal.php">Edit Data Jadwal Kelas</a>
			
				</center>
			</p>
		</div>
	</div>
</div>
</body>
</html>