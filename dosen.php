<html>
<title>CRUD</title>
<head>
<!-- pencantuman link css yang digunakan -->
<link rel="stylesheet" href="style/materialize.min.css" />
</head>
<body>
<nav>
		<div class="nav-wrapper blue">	
        <!-- <div class="container"> -->
          <a href="index.php" class="brand-logo center white-text">DOSEN</a>
        </div>
        </div>
</nav>
<!-- <div class="container" style="margin-top:8%"> -->
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
		<!-- pengaturan style conten judul --> 
			<p>
				<center>
				
				</center>
			</p>
			<br>
			<!-- pembuatan tabel dengan menggunakan style class css -->
			<p>
				<a class="btn blue" href="tambah_dosen.php">Tambah</a>
			</p>
			<table class="table table-bordered">
				<tr>
					<th>
						ID Dosen
					</th>
					<th>
						NIP
					</th>
					<th>
						Nama Dosen
					</th>
					<th>
						Prodi
					</th>
					<th>
						Fakultas
					</th>
				</tr>
				<!-- query menghubungkan ke database  -->
					<?php
						include"koneksi.php";
						$no = 1;
						$data = mysqli_query ($koneksi, " select 
																id_dosen,
																nip_dosen,
																nama_dosen,
																prodi,
																fakultas
														  from 
														  dosen 
														  order by id_dosen DESC");
						while ($row = mysqli_fetch_array ($data))
						{
					?>
					<!-- mamanggil variabel dan menampilkannya -->
				<tr>
					<td>
						<?php echo $row['id_dosen']; ?>
					</td>
					<td>
						<?php echo $row['nip_dosen']; ?>
					</td>
					<td>
						<?php echo $row['nama_dosen']; ?>
					</td>
					<td>
						<?php echo $row['prodi']; ?>
					</td>
					<td>
						<?php echo $row['fakultas']; ?>
					</td>
					<td>
					<!-- button hapus dan edit -->
						<a class="btn orange" href="edit_dosen.php?id=<?php echo $row['id_dosen']; ?>">Edit</a> 
						<a class="btn red" href="hapus_d.php?id=<?php echo $row['id_dosen']; ?>">Hapus</a>
					</td>
				</tr>
				<?php
					}
				?>
			</table>
		</div>
	</div>
</div>
</body>
</html>