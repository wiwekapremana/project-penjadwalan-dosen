<html>
<title>CRUD</title>
<!-- pencantuman link css yang digunakan -->
<head>
    <link rel="stylesheet" href="style/materialize.min.css" />
</head>

<body>
    <nav>
        <div class="nav-wrapper blue">
                <a href="index.php" class="brand-logo center white-text">EDIT DATA</a>
            </div>
        </div>
    </nav>
    <?php
	include"koneksi.php";
	$no = 1;
	$data = mysqli_query ($koneksi, " select 
											id_jadwal,
											id_dosen,
											id_kelas,
											jadwal,
                                            matakuliah
									  from 
									  jadwal_kelas 
									  where id_jadwal = $_GET[id]");
	$row = mysqli_fetch_array ($data);
	
?>
    <div class="container" style="margin-top:8%">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p>
                    <center>
                        <h5>Edit Data <?= $row['matakuliah'] ; ?></h5>
                        <hr>
                    </center>
                </p>
                <br>

                <form role="form" method="post" action="update_jadwal.php">
                    <div class="form-group">
                        <label>ID Dosen</label>
                        <input type="hidden" name="id_jadwa" value="<?php echo $row['id_jadwal'] ; ?>">
                        <input class="form-control" name="id_dosen" value="<?php echo $row['id_dosen'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>ID Kelas</label>
                        <input class="form-control" name="id_kelas" value="<?php echo $row['id_kelas'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>Jadwal</label>
                        <input class="form-control" type="date" name="jadwal" value="<?php echo $row['jadwal'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>Matakuliah</label>
                        <input class="form-control" name="matakuliah" value="<?php echo $row['matakuliah'] ; ?>">
                    </div>
                    <button type="submit" class="btn green">Perbarui</button>
                    <a href="jadwal.php" class="btn red" style="margin-right:1%;">Batal</a>
                </form>
            </div>
        </div>
    </div>
    <script src="style/materialize.min.js"></script>
</body>

</html>