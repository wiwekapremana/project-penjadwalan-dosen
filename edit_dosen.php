<html>
<title>CRUD</title>
<!-- pencantuman link css yang digunakan -->
<head>
    <link rel="stylesheet" href="style/materialize.min.css" />
</head>

<body>
    <nav>
        <div class="nav-wrapper blue">
                <a href="index.php" class="brand-logo center white-text">EDIT DATA</a>
            </div>
        </div>
    </nav>
    <?php
	include"koneksi.php";
	$no = 1;
	$data = mysqli_query ($koneksi, " select 
											id_dosen,
											foto_dosen,
											nip_dosen,
											nama_dosen,
											prodi,
                                            fakultas
									  from 
									  dosen 
									  where id_dosen = $_GET[id]");
	$row = mysqli_fetch_array ($data);
	
?>
    <div class="container" style="margin-top:8%">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <p>
                    <center>
                        <h5>Edit Data <?= $row['nama_dosen'] ; ?></h5>
                        <hr>
                    </center>
                </p>
                <br>

                <form role="form" method="post" action="update_dosen.php">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="hidden" name="id_dosen" value="<?php echo $row['id_dosen'] ; ?>">
                        <input class="form-control"type ="file" name="foto" value="<?php echo $row['foto_dosen'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>NIP</label>
                        <input class="form-control" name="nip_dosen" value="<?php echo $row['nip_dosen'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>NAMA</label>
                        <input class="form-control" name="nama_dosen" value="<?php echo $row['nama_dosen'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>Prodi</label>
                        <input class="form-control" name="prodi" value="<?php echo $row['prodi'] ; ?>">
                    </div>
                    <div class="form-group">
                        <label>Fakultas</label>
                        <input class="form-control" name="fakultas" value="<?php echo $row['fakultas'] ; ?>">
                    </div>
                    <button type="submit" class="btn green">Perbarui</button>
                    <a href="dosen.php" class="btn red" style="margin-right:1%;">Batal</a>
                </form>
            </div>
        </div>
    </div>
    <script src="style/materialize.min.js"></script>
</body>

</html>